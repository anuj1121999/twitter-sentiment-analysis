# README #

This project is used to analysis the sentiment of tweets and classify them as positive or negative.
The data has been taken from Stamford corpus.
Various Machine Learning Algorithms have been applied such as logistic regression,bernoulli naive bayes,
multinomial naive bayes.

Best results have been achieved by logistic regression.
Libraries used:sklearn,numpy,pandas.